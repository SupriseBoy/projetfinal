<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Equipe extends Model
{
    //Table name
    protected $table = 'equipes';

    protected $primaryKey = 'id_equipe';

    //Timestamps 
    public $timestamps= true;
    
    public function user(){
        return $this->belongsTo('App\User');
    }
}