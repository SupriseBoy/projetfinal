<?php

namespace App\Http\Controllers;

use App\Equipe;
use App\Joueur;
use App\Stats;

use Illuminate\Http\Request;

class AdminController extends Controller
{
    // Un controlleur qui affiche la section admin
    // Celle-ci est diviser en trois partie
    // 1: Equipe : Tout les equipes de la base de donnee
    // 1: Joueur : Tout les joueurs de la base de donnee
    // 1: Stats (Statistique) : Tout les statistique de la base de donnee

    public function index()
    {
        //Section 1 : Equipe
        $equipes = Equipe::orderby('created_at', 'asc')->get(); // Get les equipes
        session(['id_incre_equipe' => 0]); // Simplement l'affichage (compteur) du tableau qui va etre afficher sur la page

        //Section 2 : Joueur
        $joueurs = Joueur::orderby('created_at', 'asc')->get(); // Get les joueurs
        session(['id_incre_joueur' => 0]); // Simplement l'affichage (compteur) du tableau qui va etre afficher sur la page
        $array_nom_equipe = array(); //Creer un tableau de nom des equipes des joueurs
        foreach($joueurs as $joueur)
        {
            //Ajoute le nom de l'equipe du joueur
            array_push($array_nom_equipe, Equipe::where('id_equipe', $joueur->id_equipe)->get(['nom_equipe']));
        }

        //Section 3 : Stats
        $stats = Stats::orderby('created_at', 'asc')->get();// Get les stastiques
        session(['id_incre_stats' => 0]); // Simplement l'affichage (compteur) du tableau qui va etre afficher sur la page
        $array_nom_joueur = array();//Creer un tableau de nom des joueur selon leur statistique
        foreach($stats as $stat)
        {
            //Ajoute le nom du joueur selon leur statistique
            array_push($array_nom_joueur, Joueur::where('id_joueur', $stat->id_joueur)->get(['nom_joueur', 'id_equipe']));
        }

        $array_nom_equipe_stats = array();//Creer un tableau de nom des equipes des joueurs
        for($cpt = 0; $cpt < count($array_nom_joueur); $cpt ++)
        {
            //Ajoute le nom de l'equipe du joueur
            array_push($array_nom_equipe_stats, Equipe::where('id_equipe', $array_nom_joueur[$cpt][0]['id_equipe'])->get(['nom_equipe']));
        }

        return view('auth.admin', ['equipes' => $equipes, 
        'joueurs' => $joueurs, 'array_nom_equipe' => $array_nom_equipe, 
        'stats' => $stats, 'array_nom_joueur' => $array_nom_joueur,
        'array_nom_equipe_stats' => $array_nom_equipe_stats]);
    }
}
