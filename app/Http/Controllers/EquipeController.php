<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Equipe;
use App\Joueur;
use App\Stats;
use User;
use App\Mail\CreationEquipeMail;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;

class EquipeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Page principale du controlleur equipe
        $equipes = Equipe::orderBy('created_at', 'desc')->paginate(2);//Get 2 equipe 
        return view('post_equipe.index', ['equipes' => $equipes]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //Affiche la view create 
        return view('post_equipe.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Validation coter serveur
        $this->validate($request,[
            'nom_equipe' => 'required',
            'description_equipe' => 'required',
            'cover_image' => 'image|nullable|max:1999'
        ]);

        // Les conditions pour stocker une image dans le serveur (Trouver sur internet)
        if($request->hasFile('cover_image'))
        {
            // Get filename with the extension
            $filenameWithExit = $request->file('cover_image')->getClientOriginalName();
            // Get just filename
            $filename = pathinfo($filenameWithExit, PATHINFO_FILENAME);
            // Get just extension
            $extension = $request->file('cover_image')->getClientOriginalExtension();
            // Filename to store
            $fileNameToStore = $filename.'_'.time().'.'.$extension;
            // Upload Image
            $path = $request->file('cover_image')->storeAS('public/images/equipe_images', $fileNameToStore);
        }else{
            $fileNameToStore = 'noimage.jpg';
        }

        //Create Equipe
        $post_equipe = new Equipe;
        $post_equipe->nom_equipe = $request->input('nom_equipe');
        $post_equipe->description_equipe = $request->input('description_equipe');
        $post_equipe->salaire_equipe = 0;
        $post_equipe->image_equipe = $fileNameToStore;
        $post_equipe->id_user = auth()->user()->id;
        $post_equipe->save();

        Mail::to(auth()->user()->email)->send(new CreationEquipeMail($post_equipe));

        return redirect('/equipe')->with('success', 'L\'équipe a été créer');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $equipe = Equipe::find($id);//Trouve l'equipe
        session(['id_equipe' => $id]);//Stocke en session id de l'equipe pour que je l'utilise dans les autres pages
        session(['id_incre' => 0]);// Simplement l'affichage (compteur) du tableau qui va etre afficher sur la page

        $joueurs = Joueur::where('id_equipe', $equipe->id_equipe)->orderBy('created_at', 'desc')->get();
        return view('post_equipe.show', ['equipe' => $equipe, 'joueurs' => $joueurs]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $equipe = Equipe::find($id);//Trouve l'equipe
        return view('post_equipe.edit', ['equipe' => $equipe]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //Validation coter serveur
        $this->validate($request,[
            'nom_equipe' => 'required',
            'description_equipe' => 'required',
            'cover_image' => 'image|nullable|max:1999'
        ]);

         // Les conditions pour stocker une image dans le serveur (Trouver sur internet)
        if($request->hasFile('cover_image'))
        {
            // Get filename with the extension
            $filenameWithExit = $request->file('cover_image')->getClientOriginalName();
            // Get just filename
            $filename = pathinfo($filenameWithExit, PATHINFO_FILENAME);
            // Get just extension
            $extension = $request->file('cover_image')->getClientOriginalExtension();
            // Filename to store
            $fileNameToStore = $filename.'_'.time().'.'.$extension;
            // Upload Image
            $path = $request->file('cover_image')->storeAS('public/images/equipe_images', $fileNameToStore);
        }

        //Update Equipe
        $post_equipe = Equipe::find($id);
        $post_equipe->nom_equipe = $request->input('nom_equipe');
        $post_equipe->description_equipe = $request->input('description_equipe');
        if($request->hasFile('cover_image')){
            $post_equipe->cover_image = $fileNameToStore;
        } 
        $post_equipe->save();

        return redirect('/equipe')->with('success', 'L\'équipe a été modifier');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $equipe = Equipe::find($id);//Trouve l'equipe et le stocke dans un object

        // Condition qui regarde que si l'equipe contient des joueurs, 
        // va aussi supprimer les joueurs dans l'equipe
        if(count(Joueur::where('id_equipe', $id)->get()) != 0) 
        {
            $joueurs = Joueur::where('id_equipe', $id)->get();
            
            foreach($joueurs as $joueur)
            {
                $stats = Stats::where('id_joueur', $joueur->id_joueur)->get();
            } 
            // Condition qui regarde que si le joueurs contient des statistique, 
            // va aussi supprimer les statistiques du joueur
            if(count($stats) != 0) 
            {
                foreach($stats as $stat) //Supprimer tout les statistique des joueurs dans l'equipe
                {
                    $stat->delete();
                }
                foreach($joueurs as $joueur)//Supprimer tout les joueurs dans l'equipe
                {
                    $joueur->delete();
                }
            }
        }
        $equipe->delete();
        return redirect()->back()->with('success', 'L\'équipe a supprimée');
    }
}
