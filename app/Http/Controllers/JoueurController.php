<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Joueur;
use App\Stats;
use App\Equipe;
use User;

class JoueurController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Get tout les joueurs
        $joueurs = Joueur::get(['nom_joueur', 'salaire_joueur', 'pays_joueur', 'id_equipe', 'created_at', 'updated_at']);

        $array_nom_equipes = array();//Stocke les noms d'equipes des joueurs
        foreach($joueurs as $joueur)
        {
            array_push($array_nom_equipes, Equipe::where('id_equipe', $joueur->id_equipe)->get(['nom_equipe']));
        }
        return view('post_joueur.index', ['joueurs' => $joueurs, 'array_nom_equipes' => $array_nom_equipes]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('post_joueur.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //Validation coter serveur
        $this->validate($request,[
            'nom_joueur' => 'required',
            'salaire_joueur' => 'required',
            'pays_joueur' => 'required',
            'description_joueur' => 'required',
            'cover_image' => 'image|nullable|max:1999'
        ]);

        // Les conditions pour stocker une image dans le serveur (Trouver sur internet)
        if($request->hasFile('cover_image'))
        {
            // Get filename with the extension
            $filenameWithExit = $request->file('cover_image')->getClientOriginalName();
            // Get just filename
            $filename = pathinfo($filenameWithExit, PATHINFO_FILENAME);
            // Get just extension
            $extension = $request->file('cover_image')->getClientOriginalExtension();
            // Filename to store
            $fileNameToStore = $filename.'_'.time().'.'.$extension;
            // Upload Image
            $path = $request->file('cover_image')->storeAS('public/images/joueur_images', $fileNameToStore);
        }else{
            $fileNameToStore = 'noimage_joueur.jpg';
        }

        //Create Joueur
        $post_equipe = new Joueur;
        $post_equipe->nom_joueur = $request->input('nom_joueur');
        $post_equipe->description_joueur = $request->input('description_joueur');
        $post_equipe->salaire_joueur= $request->input('salaire_joueur');
        $post_equipe->pays_joueur = $request->input('pays_joueur');
        $post_equipe->image_joueur = $fileNameToStore;
        $post_equipe->id_equipe = session('id_equipe'); 
        $post_equipe->id_user = auth()->user()->id;
        $post_equipe->save();

        // La session id equipe nous permet de rediriger l'utilisateur a la page d'equipe du joueur qu'il vient de creer
        $id_equipe = session('id_equipe'); 
        return redirect("/equipe/$id_equipe")->with('success', 'Le joueur a été créer');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $joueur = Joueur::find($id);//Trouve le joueur
        session(['id_joueur' => $id]);// Stocke en session id du joueur pour que je l'utilise dans les autres pages
        session(['id_incre' => 0]); // Simplement l'affichage (compteur) du tableau qui va etre afficher sur la page

        /* Explication des lignes suivante:
           Je creer un tableau d'annee dans une session
           Ce tableau me permet juste de faire de la verification lors de l'ajouter d'une statistique d'un joueur
           La condition est que l'utilisateur ne peux pas ajouter 2 fois la meme anne de statistique pour un joueur
           Exemple: 
                    Joueur : Justin Leach 
                    Premier ajoute : Statistique->annee correspond a 2017
                    Deuxieme ajout : Statistique->annee correspond a 2018
                    Troisieme ajout : Statistique->annee correspond a 2017 !!FAUX!! 
                    Pour cette raison la creation du tableau 
        */
        $valeur_unique = Stats::where('id_joueur', $id)->select('id_annee_stats_joueur')->get();
        $array = array();
        foreach($valeur_unique as $val)
        {
            array_push($array, $val);
        }
        session(['id_annee_stats_joueur' => $array]);

        // Les statistiques de but et de passe, selection la meilleur annee
        $data_stats = Stats::where('id_joueur', $id)->orderby('nbre_points', 'desc')->get(['nbre_partie', 'nbre_buts', 'nbre_assistes', 'nbre_points', 'nbre_tir', 'annee_stats']);
         // Les stats des 5 annees les plus recentes 
        $data_stats_chart_bar = Stats::where('id_joueur', $id)->orderby('annee_stats', 'desc')->take(5)->get(['nbre_points', 'nbre_tir', 'annee_stats']);

        // Les statistique du joueur
        $data_stats_joueur = Stats::where('id_joueur', $id)->orderby('annee_stats', 'desc')->get();

        return view('post_joueur.show', ['joueur' => $joueur, 'data_stats' => $data_stats, 'data_stats_chart_bar' => $data_stats_chart_bar, 'data_stats_joueur' => $data_stats_joueur]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $joueur = Joueur::find($id);
        return view('post_joueur.edit', ['joueur' => $joueur]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'nom_joueur' => 'required',
            'salaire_joueur' => 'required',
            'pays_joueur' => 'required',
            'description_joueur' => 'required',
            'cover_image' => 'image|nullable|max:1999'
        ]);

        if($request->hasFile('cover_image'))
        {
            // Get filename with the extension
            $filenameWithExit = $request->file('cover_image')->getClientOriginalName();
            // Get just filename
            $filename = pathinfo($filenameWithExit, PATHINFO_FILENAME);
            // Get just extension
            $extension = $request->file('cover_image')->getClientOriginalExtension();
            // Filename to store
            $fileNameToStore = $filename.'_'.time().'.'.$extension;
            // Upload Image
            $path = $request->file('cover_image')->storeAS('public/images/joueur_images', $fileNameToStore);
        }

        //Update Joueur
        $post_equipe = Joueur::find($id);
        $post_equipe->nom_joueur = $request->input('nom_joueur');
        $post_equipe->description_joueur = $request->input('description_joueur');
        $post_equipe->salaire_joueur= $request->input('salaire_joueur');
        $post_equipe->pays_joueur = $request->input('pays_joueur');
        if($request->hasFile('cover_image')){
            $post_equipe->image_joueur = $fileNameToStore;
        } 
        $post_equipe->save();

        $id_equipe = session('id_equipe');
        return redirect("/equipe/$id_equipe")->with('success', 'Le joueur a été modifier');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //Meme concep expliquer dans le controlleur Equipe dans la fonction destroy
        
        $joueur = Joueur::find($id);

        $stats = Stats::where('id_joueur', $id)->get();

        foreach($stats as $stat)
        {
            $stat->delete();
        }
        $joueur->delete();
        
        return redirect()->back()->with('success', 'Le joueur a été supprimer');
    }
}
