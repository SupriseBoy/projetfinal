<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class LangueController extends Controller
{
    //
    public function index($langue)
    {
        session(['lang' => $langue]);
        return redirect()->back();
    }
}
