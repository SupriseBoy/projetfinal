<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Joueur;
use App\Stats;
use User;

class StatsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $stats = Stats::get(['nbre_partie', 'nbre_buts', 'nbre_assistes', 'nbre_points', 'nbre_tir', 'annee_stats', 'id_joueur']);

        $array_nom_joueur = array();
        foreach($stats as $stat)
        {
            array_push($array_nom_joueur, Joueur::where('id_joueur', $stat->id_joueur)->get(['nom_joueur']));
        }
        return view('post_stats.index', ['stats' => $stats, 'array_nom_joueur' => $array_nom_joueur]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('post_stats.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'nbre_partie' => 'required|integer|min:1',
            'nbre_buts' => 'required|integer|min:0',
            'nbre_assistes' => 'required|integer|min:0',
            'nbre_tir' => 'required|integer|min:1',
            'annee_stats' => 'required|integer|min:4'
        ]);
        $value = session('id_annee_stats_joueur');

        // Explication dans le controlleur Joueur dans la fonction show
        if(array_search(strval(session('id_joueur')).$request->input('annee_stats'), array_column($value, 'id_annee_stats_joueur')) !== false) {
            return redirect()->back()->with('error', "The year of stats of the player is already taken. {{$request->input('annee_stats')}}");
        }else{
            //Create Stats of the player
            $post_stats = new Stats;
            $post_stats->nbre_partie = $request->input('nbre_partie');
            $post_stats->nbre_buts = $request->input('nbre_buts');
            $post_stats->nbre_assistes = $request->input('nbre_assistes');
            $post_stats->nbre_points= $post_stats['nbre_buts'] + $post_stats['nbre_assistes'];
            $post_stats->nbre_tir = $request->input('nbre_tir');
            $post_stats->annee_stats = $request->input('annee_stats');
            $post_stats->id_annee_stats_joueur = strval(session('id_joueur')).$request->input('annee_stats');
            $post_stats->id_joueur = session('id_joueur');
            $post_stats->id_equipe = session('id_equipe'); 
            $post_stats->id_user = auth()->user()->id;
            $post_stats->save();

            //Rajoute la nouvelle annee de statistique dans la session
            $id_joueur = session('id_joueur');
            $valeur_unique = Stats::where('id_joueur', $id_joueur)->select('id_annee_stats_joueur')->get();
            $array = array();
            foreach($valeur_unique as $val)
            {
                array_push($array, $val);
            }
            session(['id_annee_stats_joueur' => $array]);
            
            return redirect("/joueur/$id_joueur")->with('success', 'Les statistique personnelle du joueur a été créer');  
        }
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $stats = Stats::find($id);//Touve la statistique
        return view('post_stats.edit', ['stats' => $stats]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'nbre_partie' => 'required|integer|min:1',
            'nbre_buts' => 'required|integer|min:0',
            'nbre_assistes' => 'required|integer|min:0',
            'nbre_tir' => 'required|integer|min:1',
            'annee_stats' => 'required|integer|min:4'
        ]);
        $value = session('id_annee_stats_joueur');

        if(array_search(strval(session('id_joueur')).$request->input('annee_stats'), array_column($value, 'id_annee_stats_joueur')) !== false) {
            return redirect()->back()->with('error', "The year of stats of the player is already taken. {{$request->input('annee_stats')}}");
        }else{
                //Modifier  les statistique du joueur en question
                $post_stats = Stats ::find($id);
                $post_stats->nbre_partie = $request->input('nbre_partie');
                $post_stats->nbre_buts = $request->input('nbre_buts');
                $post_stats->nbre_assistes = $request->input('nbre_assistes');
                $post_stats->nbre_points= $post_stats['nbre_buts'] + $post_stats['nbre_assistes'];
                $post_stats->nbre_tir = $request->input('nbre_tir');
                $post_stats->annee_stats = $request->input('annee_stats');
                $post_stats->id_annee_stats_joueur = strval(session('id_joueur')).$request->input('annee_stats');
                $post_stats->save();

                $id_joueur = session('id_joueur');
                $valeur_unique = Stats::where('id_joueur', $id_joueur)->select('id_annee_stats_joueur')->get();
                $array = array();
                foreach($valeur_unique as $val)
                {
                    array_push($array, $val);
                }
                session(['id_annee_stats_joueur' => $array]);
                
                return redirect("/joueur/$id_joueur")->with('success', 'Les statistique personnelle du joueur a été modifier');  
            }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $stats = Stats::find($id);
        $stats->delete();
        return redirect()->back()->with('success', 'Le statistique a été supprimer');
    }
}
