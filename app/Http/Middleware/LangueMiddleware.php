<?php

namespace App\Http\Middleware;

use Closure;

class LangueMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        app()->setlocale(session('lang'));
        return $next($request);
    }
}
