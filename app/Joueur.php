<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Joueur extends Model
{
    //Table name
    protected $table = 'joueurs';

    protected $primaryKey = 'id_joueur';

    //Timestamps 
    public $timestamps= true;
    
    public function user(){
        return $this->belongsTo('App\User');
    }
}