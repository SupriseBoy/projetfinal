<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Equipe;

class CreationEquipeMail extends Mailable
{
    use Queueable, SerializesModels;

    public $nom_equipe;
    public $description_equipe;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Equipe $equipe)
    {
        $this->nom_equipe = $equipe->nom_equipe;
        $this->description_equipe = $equipe->description_equipe;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
         return $this->view('mail.index')->from("cacamiel@hotmail.com")->subject("Vous avez créer un équipe")->with(["nom_equipe" => $this->nom_equipe, "description_equipe" => $this->description_equipe]);
    }
}
