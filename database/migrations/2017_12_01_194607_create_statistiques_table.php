<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStatistiquesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('statistiques', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('nbre_partie');
            $table->integer('nbre_buts');
            $table->integer('nbre_assistes');
            $table->integer('nbre_points');
            $table->integer('nbre_tir');
            $table->string('annee_stats');
            $table->string('id_annee_stats_joueur')->unique();
            $table->integer('id_joueur');
            $table->integer('id_equipe');
            $table->integer('id_user');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('statistiques');
    }
}
