$(document).ready(function() {
    $('#datatable_equipe_players').DataTable( {
        "lengthMenu": [],
        "pageLength": 3,
        "bLengthChange": false,
        "bFilter": false,
    } );
} );

$(document).ready(function() {
    $('#data_search').DataTable({
        "bPaginate": true,
        "bLengthChange": false,
        "bFilter": true,
        "bInfo": true,
        "bAutoWidth": false,
        "bFilter": "width:100%"
    });
    $('.dataTables_filterinput[type="search"]').css(
        {'width':'985px','display':'inline-block'}
     );
});
