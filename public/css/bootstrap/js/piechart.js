if(document.getElementById("pieChart") != null)
{
    var ctx = document.getElementById("pieChart").getContext('2d');
    var pieChart_goal = $('#pieChart_goal').val().trim();
    var pieChart_assistes = $('#pieChart_assistes').val().trim();
    var myPieChart = new Chart(ctx, {
        type: 'pie',
        data: {
            labels: ["Goal", "Assits"],
            datasets: [
                {
                    data: [pieChart_goal, pieChart_assistes],
                    backgroundColor: ["#F7464A", "#46BFBD"],
                    hoverBackgroundColor: ["#FF5A5E", "#5AD3D1"]
                }
            ]
        },
        options: {
            responsive: true,
            width:500,
            height:300,
        }
    
    });    
}


if($('#barChartPoint_0').val() != null)
{
    if($('#barChartPoint_1').val() != null)
    {
        if($('#barChartPoint_2').val() != null)
        {
            if($('#barChartPoint_3').val() != null)
            {
                if($('#barChartPoint_4').val() != null)
                {
                    var ctxBPoint = document.getElementById("barChartPoint").getContext('2d');
                    var barChartPoint_0 = $('#barChartPoint_0').val().trim();
                    var barChartAnnee_0 = $('#barChartAnnee_0').val().trim();
                    var barChartPoint_1 = $('#barChartPoint_1').val().trim();
                    var barChartAnnee_1 = $('#barChartAnnee_1').val().trim();
                    var barChartPoint_2 = $('#barChartPoint_2').val().trim();
                    var barChartAnnee_2 = $('#barChartAnnee_2').val().trim();
                    var barChartPoint_3 = $('#barChartPoint_3').val().trim();
                    var barChartAnnee_3 = $('#barChartAnnee_3').val().trim();
                    var barChartPoint_4 = $('#barChartPoint_4').val().trim();
                    var barChartAnnee_4 = $('#barChartAnnee_4').val().trim();
        
                    var myBarChart = new Chart(ctxBPoint, {
                        type: 'bar',
                        data: {
                            labels: [barChartAnnee_0, barChartAnnee_1, barChartAnnee_2, barChartAnnee_3, barChartAnnee_4],
                            datasets: [{
                                label: '# of Points',
                                data: [barChartPoint_0, barChartPoint_1, barChartPoint_2, barChartPoint_3, barChartPoint_4],
                                backgroundColor: [
                                    'rgba(153,102, 255, 0.2)',
                                    'rgba(153,102, 255, 0.2)',
                                    'rgba(153, 102, 255, 0.2)',
                                    'rgba(153, 102, 255, 0.2)',
                                    'rgba(153, 102, 255, 0.2)'
                                ],
                                borderColor: [
                                    'rgba(153, 102, 255, 1)',
                                    'rgba(153, 102, 255, 1)',
                                    'rgba(153, 102, 255, 1)',
                                    'rgba(153, 102, 255, 1)',
                                    'rgba(153, 102, 255, 1)'
                                ],
                                borderWidth: 1
                            }]
                        },
                        optionss: {
                            scales: {
                                yAxes: [{
                                    ticks: {
                                        beginAtZero:true
                                    }
                                }]
                            }
                        }
                    });
                    
                }else{
                    var ctxBPoint = document.getElementById("barChartPoint").getContext('2d');
                    var barChartPoint_0 = $('#barChartPoint_0').val().trim();
                    var barChartAnnee_0 = $('#barChartAnnee_0').val().trim();
                    var barChartPoint_1 = $('#barChartPoint_1').val().trim();
                    var barChartAnnee_1 = $('#barChartAnnee_1').val().trim();
                    var barChartPoint_2 = $('#barChartPoint_2').val().trim();
                    var barChartAnnee_2 = $('#barChartAnnee_2').val().trim();
                    var barChartPoint_3 = $('#barChartPoint_3').val().trim();
                    var barChartAnnee_3 = $('#barChartAnnee_3').val().trim();
        
                    var myBarChart = new Chart(ctxBPoint, {
                        type: 'bar',
                        data: {
                            labels: [barChartAnnee_0, barChartAnnee_1, barChartAnnee_2, barChartAnnee_3],
                            datasets: [{
                                label: '# of Points',
                                data: [barChartPoint_0, barChartPoint_1, barChartPoint_2, barChartPoint_3],
                                backgroundColor: [
                                    'rgba(153,102, 255, 0.2)',
                                    'rgba(153, 102, 255, 0.2)',
                                    'rgba(153, 102, 255, 0.2)',
                                    'rgba(153, 102, 255, 0.2)'
                                ],
                                borderColor: [
                                    'rgba(153, 102, 255, 1)',
                                    'rgba(153, 102, 255, 1)',
                                    'rgba(153, 102, 255, 1)',
                                    'rgba(153, 102, 255, 1)'
                                ],
                                borderWidth: 1
                            }]
                        },
                        optionss: {
                            scales: {
                                yAxes: [{
                                    ticks: {
                                        beginAtZero:true
                                    }
                                }]
                            }
                        }
                    });
                }
            }else{
                var ctxBPoint = document.getElementById("barChartPoint").getContext('2d');
                var barChartPoint_0 = $('#barChartPoint_0').val().trim();
                var barChartAnnee_0 = $('#barChartAnnee_0').val().trim();
                var barChartPoint_1 = $('#barChartPoint_1').val().trim();
                var barChartAnnee_1 = $('#barChartAnnee_1').val().trim();
                var barChartPoint_2 = $('#barChartPoint_2').val().trim();
                var barChartAnnee_2 = $('#barChartAnnee_2').val().trim();
    
                var myBarChart = new Chart(ctxBPoint, {
                    type: 'bar',
                    data: {
                        labels: [barChartAnnee_0, barChartAnnee_1, barChartAnnee_2],
                        datasets: [{
                            label: '# of Points',
                            data: [barChartPoint_0, barChartPoint_1, barChartPoint_2],
                            backgroundColor: [
                                'rgba(153, 102, 255, 0.2)',
                                'rgba(153, 102, 255, 0.2)',
                                'rgba(153, 102, 255, 0.2)'
                            ],
                            borderColor: [
                                'rgba(153, 102, 255, 1)',
                                'rgba(153, 102, 255, 1)',
                                'rgba(153, 102, 255, 1)'
                            ],
                            borderWidth: 1
                        }]
                    },
                    optionss: {
                        scales: {
                            yAxes: [{
                                ticks: {
                                    beginAtZero:true
                                }
                            }]
                        }
                    }
                });
            }
        }else{
            var ctxBPoint = document.getElementById("barChartPoint").getContext('2d');
            var barChartPoint_0 = $('#barChartPoint_0').val().trim();
            var barChartAnnee_0 = $('#barChartAnnee_0').val().trim();
            var barChartPoint_1 = $('#barChartPoint_1').val().trim();
            var barChartAnnee_1 = $('#barChartAnnee_1').val().trim();

            var myBarChart = new Chart(ctxBPoint, {
                type: 'bar',
                data: {
                    labels: [barChartAnnee_0, barChartAnnee_1],
                    datasets: [{
                        label: '# of Points',
                        data: [barChartPoint_0, barChartPoint_1],
                        backgroundColor: [
                            'rgba(153, 102, 255, 0.2)',
                            'rgba(153, 102, 255, 0.2)'
                        ],
                        borderColor: [
                            'rgba(153, 102, 255, 1)',
                            'rgba(153, 102, 255, 1)'
                        ],
                        borderWidth: 1
                    }]
                },
                optionss: {
                    scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero:true
                            }
                        }]
                    }
                }
            });
        }   
    }else{
        var ctxBPoint = document.getElementById("barChartPoint").getContext('2d');
        var barChartPoint_0 = $('#barChartPoint_0').val().trim();
        var barChartAnnee_0 = $('#barChartAnnee_0').val().trim();

        var myBarChart = new Chart(ctxBPoint, {
            type: 'bar',
            data: {
                labels: [barChartAnnee_0],
                datasets: [{
                    label: '# of Points',
                    data: [barChartPoint_0],
                    backgroundColor: [
                        'rgba(153, 102, 255, 0.2)'
                    ],
                    borderColor: [
                        'rgba(153, 102, 255, 1)'
                    ],
                    borderWidth: 1
                }]
            },
            optionss: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero:true
                        }
                    }]
                }
            }
        });
    }
}

if($('#barChartShot_0').val() != null)
{
    if($('#barChartShot_1').val() != null)
    {
        if($('#barChartShot_2').val() != null)
        {
            if($('#barChartShot_3').val() != null)
            {
                if($('#barChartShot_4').val() != null)
                {
                    var ctxShot = document.getElementById("barChartShot").getContext('2d');
                    var barChartShot_0 = $('#barChartShot_0').val().trim();
                    var barChartAnnee_0 = $('#barChartAnnee_0').val().trim();
                    var barChartShot_1 = $('#barChartShot_1').val().trim();
                    var barChartAnnee_1 = $('#barChartAnnee_1').val().trim();
                    var barChartShot_2 = $('#barChartShot_2').val().trim();
                    var barChartAnnee_2 = $('#barChartAnnee_2').val().trim();
                    var barChartShot_3 = $('#barChartShot_3').val().trim();
                    var barChartAnnee_3 = $('#barChartAnnee_3').val().trim();
                    var barChartShot_4 = $('#barChartShot_4').val().trim();
                    var barChartAnnee_4 = $('#barChartAnnee_4').val().trim();
        
                    var myBarChart = new Chart(ctxShot, {
                        type: 'bar',
                        data: {
                            labels: [barChartAnnee_0, barChartAnnee_1, barChartAnnee_2, barChartAnnee_3,barChartAnnee_4],
                            datasets: [{
                                label: '# of Shots',
                                data: [barChartShot_0, barChartShot_1, barChartShot_2, barChartShot_3, barChartShot_4],
                                backgroundColor: [
                                    'rgba(255, 159, 64, 0.2)',
                                    'rgba(255, 159, 64, 0.2)',                               
                                    'rgba(255, 159, 64, 0.2)',
                                    'rgba(255, 159, 64, 0.2)',
                                    'rgba(255, 159, 64, 0.2)'
                                ],
                                borderColor: [
                                    'rgba(255, 159, 64, 1)',
                                    'rgba(255, 159, 64, 1)',
                                    'rgba(255, 159, 64, 1)',
                                    'rgba(255, 159, 64, 1)',
                                    'rgba(255, 159, 64, 1)'
                                ],
                                borderWidth: 1
                            }]
                        },
                        optionss: {
                            scales: {
                                yAxes: [{
                                    ticks: {
                                        beginAtZero:true
                                    }
                                }]
                            }
                        }
                    });
                }else{
                    var ctxShot = document.getElementById("barChartShot").getContext('2d');
                    var barChartShot_0 = $('#barChartShot_0').val().trim();
                    var barChartAnnee_0 = $('#barChartAnnee_0').val().trim();
                    var barChartShot_1 = $('#barChartShot_1').val().trim();
                    var barChartAnnee_1 = $('#barChartAnnee_1').val().trim();
                    var barChartShot_2 = $('#barChartShot_2').val().trim();
                    var barChartAnnee_2 = $('#barChartAnnee_2').val().trim();
                    var barChartShot_3 = $('#barChartShot_3').val().trim();
                    var barChartAnnee_3 = $('#barChartAnnee_3').val().trim();
        
                    var myBarChart = new Chart(ctxShot, {
                        type: 'bar',
                        data: {
                            labels: [barChartAnnee_0, barChartAnnee_1, barChartAnnee_2, barChartAnnee_3],
                            datasets: [{
                                label: '# of Shots',
                                data: [barChartShot_0, barChartShot_1, barChartShot_2, barChartShot_3],
                                backgroundColor: [
                                    'rgba(255, 159, 64, 0.2)',                               
                                    'rgba(255, 159, 64, 0.2)',
                                    'rgba(255, 159, 64, 0.2)',
                                    'rgba(255, 159, 64, 0.2)'
                                ],
                                borderColor: [
                                    'rgba(255, 159, 64, 1)',
                                    'rgba(255, 159, 64, 1)',
                                    'rgba(255, 159, 64, 1)',
                                    'rgba(255, 159, 64, 1)'
                                ],
                                borderWidth: 1
                            }]
                        },
                        optionss: {
                            scales: {
                                yAxes: [{
                                    ticks: {
                                        beginAtZero:true
                                    }
                                }]
                            }
                        }
                    });
                }
            }else{
                var ctxShot = document.getElementById("barChartShot").getContext('2d');
                var barChartShot_0 = $('#barChartShot_0').val().trim();
                var barChartAnnee_0 = $('#barChartAnnee_0').val().trim();
                var barChartShot_1 = $('#barChartShot_1').val().trim();
                var barChartAnnee_1 = $('#barChartAnnee_1').val().trim();
                var barChartShot_2 = $('#barChartShot_2').val().trim();
                var barChartAnnee_2 = $('#barChartAnnee_2').val().trim();
    
                var myBarChart = new Chart(ctxShot, {
                    type: 'bar',
                    data: {
                        labels: [barChartAnnee_0, barChartAnnee_1, barChartAnnee_2],
                        datasets: [{
                            label: '# of Shots',
                            data: [barChartShot_0, barChartShot_1, barChartShot_2],
                            backgroundColor: [
                                'rgba(255, 159, 64, 0.2)',
                                'rgba(255, 159, 64, 0.2)',
                                'rgba(255, 159, 64, 0.2)'
                            ],
                            borderColor: [
                                'rgba(255, 159, 64, 1)',
                                'rgba(255, 159, 64, 1)',
                                'rgba(255, 159, 64, 1)'
                            ],
                            borderWidth: 1
                        }]
                    },
                    optionss: {
                        scales: {
                            yAxes: [{
                                ticks: {
                                    beginAtZero:true
                                }
                            }]
                        }
                    }
                });
            }
        }else{
            var ctxShot = document.getElementById("barChartShot").getContext('2d');
            var barChartShot_0 = $('#barChartShot_0').val().trim();
            var barChartAnnee_0 = $('#barChartAnnee_0').val().trim();
            var barChartShot_1 = $('#barChartShot_1').val().trim();
            var barChartAnnee_1 = $('#barChartAnnee_1').val().trim();

            var myBarChart = new Chart(ctxShot, {
                type: 'bar',
                data: {
                    labels: [barChartAnnee_0, barChartAnnee_1],
                    datasets: [{
                        label: '# of Shots',
                        data: [barChartShot_0, barChartShot_1],
                        backgroundColor: [
                            'rgba(255, 159, 64, 0.2)',
                            'rgba(255, 159, 64, 0.2)'
                        ],
                        borderColor: [
                            'rgba(255, 159, 64, 1',
                            'rgba(255, 159, 64, 1)'
                        ],
                        borderWidth: 1
                    }]
                },
                optionss: {
                    scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero:true
                            }
                        }]
                    }
                }
            });
            }
    }else{
        var ctxShot = document.getElementById("barChartShot").getContext('2d');
        var barChartShot_0 = $('#barChartShot_0').val().trim();
        var barChartAnnee_0 = $('#barChartAnnee_0').val().trim();

        var myBarChart = new Chart(ctxShot, {
            type: 'bar',
            data: {
                labels: [barChartAnnee_0],
                datasets: [{
                    label: '# of Shots',
                    data: [barChartShot_0],
                    backgroundColor: [
                        'rgba(255, 159, 64, 1)'
                    ],
                    borderColor: [
                        'rgba(255, 159, 64, 01)'
                    ],
                    borderWidth: 1
                }]
            },
            optionss: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero:true
                        }
                    }]
                }
            }
        });
    }
}