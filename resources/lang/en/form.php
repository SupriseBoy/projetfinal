<?php
return [
    'Navbar_Accueil' => 'Home',
    'Navbar_Connexion' => 'Login',
    'Navbar_Deconnexion' => 'Logout',
    'Navbar_Inscription' => 'Register',
    'Navbar_Langue' => 'Language',
    'Navbar_Admin' => 'Admin Section',

    'Button_ajouter' => 'Create',
    'Button_Modfier' => 'Edit',
    'Button_Modfier_' => 'Update',
    'Button_Supprimer' => 'Delete',
    'Button_Ajouter_equipe' => 'Add a team',
    'Button_Ajouter_joueur' => 'Add a player',

    'Form_Liste_equipe' => 'Team Information',
    'Form__equipe' => 'Mandatory information for the team',
    'Form__equipe_nom' => 'Team name',
    'Form__equipe_description' => 'Team description',
    'Form__equipe_image' => 'Image of the team',

    'Form_equipe_aucun' => 'No team found',
    'Form_joueur_aucun' => 'No player found',
    'Form_stats_aucun' => 'No stats found',

    'Form_Liste_joueur' => 'Player(s) Information List',
    'Form_Liste_joueurs' => 'List of the players',
    'Form_joueur' => 'Personal information of the player',
    'Form_Nom' => 'Player Name',
    'Form_Salaire' => 'The Salary',
    'Form_Pays' => 'The country',
    'Form_Description' => 'Player description',
    'Form_joueur_image' => 'Player Image',


    'Form_admin_section' => 'Welcome to the admin section',
    'Form_admin_section_equipe' => 'Team',
    'Form_admin_section_equipe_button' => 'CHECK EACH TEAM',
    'Form_admin_section_equipe_desc' => 'Find your team, your team\'s rivals and everything you need to know at your fingertips.',
    'Form_admin_section_joueur' => 'Player',
    'Form_admin_section_joueur_button' => 'BEST PLAYERS',
    'Form_admin_section_joueur_desc' => 'Who\'s hot and who\'s not.',
    'Form_admin_section_stats' => 'Statistics',
    'Form_admin_section_stats_desc' => 'Track the indicators that predict performance.',
    'Form_admin_section_stats_button' => 'FIND WINNERS AVOID LOSERS',
    'Form_admin_section_stats_desc' => 'Team',
    'Form_admin_section_stats_aucun' => 'No statistics in the database',

    'Form_email' => 'Your email',
    'Form_nom_user' => 'Your name',
    'Form_motdepasse' => 'Your password',
    'Form_souvenir' => 'Remember password',
    'Form_motdepasse_oublier' => 'Forgot your password ?',
    'Form_motdepasse_confir' => 'Confirm your password',
    'Form_reset' => 'Reset your password',
    'Form_Nouveau_motdepasse' => 'New Password',

    'Form_annee' => 'Year',

    'Form_Analytics' => 'Analytics',
    'Form_Analytics_description' => 'HOT STATS - Track the statistics that predict the future.',
    'Button_Analytics' => 'FIND WINNERS AVOID LOSERS',

    'Form_Blog' => 'Blog',
    'Form_Blog_description' => 'HOCKEY TALK - Replay the game or tell us who will win the next one.',
    'Button_Blog' => 'Comming soon',

    'Form_home_description_titre' => 'TEAM CENTRAL',

    'Form_Stats_desc_1' => 'The best year in',
    'Form_Stats_desc_2_1' => 'The last',
    'Form_Stats_desc_2_2' => 'year(s)',

    'Form_stats_list' => 'List of Stats',
    'Form_stats_button' => 'Create Stats',
    'Form_stats_game' => 'Number of game played',
    'Form_stats_but' => 'Number of goals',
    'Form_stats_assite' => 'Number of assists',
    'Form_stats_tir' => 'Number of shot',
    'Form_stats_annee' => 'Season year',

    'Form_image_pro' => 'Protected Image',
    'Form_Creer' => 'Create at',
    'Form_Misjour' => 'Updated at'
    ];