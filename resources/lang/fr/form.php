<?php
return [
    'Navbar_Accueil' => 'Accueil',
    'Navbar_Connexion' => 'Connexion',
    'Navbar_Deconnexion' => 'Deconnexion',
    'Navbar_Inscription' => 'Inscription',
    'Navbar_Langue' => 'Langue',
    'Navbar_Admin' => 'Section Admin ',

    'Button_ajouter' => 'Ajouter',
    'Button_Modfier' => 'Modifier',
    'Button_Modfier_' => 'Mettre à jour',
    'Button_Supprimer' => 'Supprimer',
    'Button_Ajouter_equipe' => 'Ajouter une équipe',
    'Button_Ajouter_joueur' => 'Ajouter un joueur',

    'Form_Liste_equipe' => 'Liste d\'information de l\'équipe',
    'Form__equipe' => 'Information obligatoire pour l\'équipe',
    'Form__equipe_nom' => 'Nom de l\'équipe',
    'Form__equipe_description' => 'Description équipe',
    'Form__equipe_image' => 'Image de l\'équipe',

    'Form_equipe_aucun' => 'Aucune équipe trouvée',
    'Form_joueur_aucun' => 'Aucun joueur trouvée',
    'Form_stats_aucun' => 'Aucune statistique trouvée',

    'Form_Liste_joueur' => 'Liste des joueurs dans l\'équipe',
    'Form_Liste_joueurs' => 'Liste des joueurs',
    'Form_joueur' => 'Les information personnel du joueur',
    'Form_Nom' => 'Nom du joueur',
    'Form_Salaire' => 'Le salaire',
    'Form_Pays' => 'Le pays',
    'Form_Description' => 'Description du joueur',
    'Form_joueur_image' => 'Image du joueur',


    'Form_admin_section' => 'Bienvenue dans la section admin',
    'Form_admin_section_equipe' => 'Équipe',
    'Form_admin_section_equipe_button' => 'VÉRIFIER CHAQUE ÉQUIPE',
    'Form_admin_section_equipe_desc' => 'Trouvez votre équipe, les rivaux de votre équipe et tout ce que vous devez savoir à portée de main.',
    'Form_admin_section_joueur' => 'Joueur',
    'Form_admin_section_joueur_button' => 'MEILLEURS JOUEURS',
    'Form_admin_section_joueur_desc' => 'Qui est bon et qui ne l\'est pas.',
    'Form_admin_section_stats' => 'Statistiques',
    'Form_admin_section_stats_desc' => 'Suivez les indicateurs qui prédisent la performance.',
    'Form_admin_section_stats_button' => 'TROUVER DES GAGNANTS ÉVITER DES PERDANTS',
    'Form_admin_section_stats_desc' => 'Qui est bon et qui ne l\'est pas.',
    'Form_admin_section_stats_aucun' => 'Aucune statistiques dans la base de donnée',
    

    'Form_email' => 'Votre courriel',
    'Form_nom_user' => 'Votre nom',
    'Form_motdepasse' => 'Votre mot de passe',
    'Form_souvenir' => 'Mémoriser le mot de passe',
    'Form_motdepasse_oublier' => 'Mot de passe oublié ?',
    'Form_motdepasse_confir' => 'Confirmer votre mot de passe',
    'Form_reset' => 'Réinitialiser votre mot de passe',
    'Form_Nouveau_motdepasse' => 'Nouveau mot de passe',

    'Form_annee' => 'Année',
    
    'Form_Analytics' => 'Analytique',
    'Form_Analytics_description' => 'Statistiques - Suivez les statistiques qui prédisent l\'avenir.',
    'Button_Analytics' => 'TROUVER DES GAGNANTS ÉVITER DES PERDANTS',

    'Form_Blog' => 'Blog',
    'Form_Blog_description' => 'PARLER HOCKEY - Rejouez le jeu ou dites-nous qui va gagner le prochain.',
    'Button_Blog' => 'Arrive bientôt',

    'Form_home_description_titre' => 'ÉQUIPE CENTRALE',

    
    'Form_Stats_desc_1' => 'La meilleure année',
    'Form_Stats_desc_2_1' => 'La dernière',
    'Form_Stats_desc_2_2' => 'année(s)',

    'Form_stats_list' => 'Liste des statistiques',
    'Form_stats_button' => 'Créer des statistiques',
    'Form_stats_titre' => 'Stastistique du joueur',
    'Form_stats_game' => 'Nombre de parties jouées',
    'Form_stats_but' => 'Nombre de buts',
    'Form_stats_assite' => 'Nombre de passes',
    'Form_stats_tir' => 'Nombre de tir',
    'Form_stats_annee' => 'Année de la saison',

    'Form_image_pro' => 'Image protégée',

    'Form_Creer' => 'Créer le',
    'Form_Misjour' => 'Mis à jour à'
    ];