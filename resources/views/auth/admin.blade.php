@extends('layouts.app') 
@section('content')
<div class="card">
    <div class="card-header orange white-text text-center">
        <h1>{{__('form.Form_admin_section')}}</h1>
    </div>
    <div class="card-body">
        <div class="card">
            <div class="card-header customcolor white-text text-center">
                <h1>Section : {{__('form.Form_admin_section_equipe')}}</h1>
            </div>
            <div class="card-body">
                @if(count($equipes) > 0)
                <div class="table-wrapper-2">
                    <!--Table-->
                    <table class="table table-responsive text-center">
                        <thead class="mdb-color lighten-4">
                            <tr>
                                <th>#</th>
                                <th class="th-lg">{{__('form.Form__equipe_nom')}}</th>
                                <th class="th-lg">{{__('form.Form_Creer')}}</th>
                                <th class="th-lg">{{__('form.Form_Misjour')}}</th>
                                <th class="th-lg"></th>
                                <th class="th-lg"></th>
                                <th class="th-lg"></th>
                                <th class="th-lg"></th>
                            </tr>
                        </thead>
                        <tbody>
                                @foreach($equipes as $equipe)
                                <tr>
                                    <th scope="row">
                                        <?php
                                            $value = session('id_incre_equipe');
                                            $value++;
                                            session(['id_incre_equipe' => $value]);
                                            echo(session('id_incre_equipe'));
                                        ?>
                                    </th>
                                    <td>{{$equipe->nom_equipe}}</td>
                                    <td>{{$equipe->created_at}}</td>
                                    <td>{{$equipe->updated_at}}</td>
                                    <td><form action="/equipe/{{$equipe->id_equipe}}/edit"><button type="submit" id="buttonplayersize" class="btn btn-info btn-sm">{{__('form.Button_Modfier')}}</button></form></td>
                                    <td>
                                        <form action="{{ action('EquipeController@destroy', $equipe->id_equipe)}} " method="post" class="pull-right">
                                            {{ method_field('DELETE') }}
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                            <button id="buttonplayersize" type="sumbit" class="btn btn-danger btn-sm">{{__('form.Button_Supprimer')}}</button>
                                        </form>
                                    </td>
                                </tr>
                                @endforeach
                        </tbody>
                    </table>
                    <!--Table-->
                </div>
                @else
                <p>{{__('form.Form_joueur_aucun')}}</p>
                @endif
            </div>
        </div>
        <hr>
        <div class="card">
        <div class="card-header purple white-text text-center">
            <h1>Section : {{__('form.Form_admin_section_joueur')}}</h1>
        </div>
        <div class="card-body">
            @if(count($joueurs) > 0)
            <div class="table-wrapper-2">
                <!--Table-->
                <table class="table table-responsive text-center">
                    <thead class="mdb-color lighten-4">
                        <tr>
                            <th>#</th>
                            <th class="th-lg">{{__('form.Form__equipe_nom')}}</th>
                            <th class="th-lg">{{__('form.Form_Nom')}}</th>
                            <th class="th-lg">{{__('form.Form_Creer')}}</th>
                            <th class="th-lg">{{__('form.Form_Misjour')}}</th>
                            <th class="th-lg"></th>
                            <th class="th-lg"></th>
                            <th class="th-lg"></th>
                            <th class="th-lg"></th>
                        </tr>
                    </thead>
                    <tbody>
                            @for($cpt = 0; $cpt < count($joueurs); $cpt++)
                            <tr>
                                <th scope="row">
                                    <?php
                                        $value = session('id_incre_joueur');
                                        $value++;
                                        session(['id_incre_joueur' => $value]);
                                        echo(session('id_incre_joueur'));
                                    ?>
                                </th>
                                <td>{{$array_nom_equipe[$cpt][0]['nom_equipe']}}</td>
                                <td>{{$joueurs[$cpt]->nom_joueur}}</td>
                                <td>{{$joueurs[$cpt]->created_at}}</td>
                                <td>{{$joueurs[$cpt]->updated_at}}</td>
                                <td><form action="/joueur/{{$joueurs[$cpt]->id_joueur}}/edit"><button type="submit" id="buttonplayersize" class="btn btn-info btn-sm">{{__('form.Button_Modfier')}}</button></form></td>
                                <td>
                                    <form action="{{ action('JoueurController@destroy', $joueurs[$cpt]->id_joueur) }}" method="post" class="pull-right">
                                        {{ method_field('DELETE') }}
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <button id="buttonplayersize" type="sumbit" class="btn btn-danger btn-sm">{{__('form.Button_Supprimer')}}</button>
                                    </form>
                                </td>
                            </tr>
                            @endfor
                    </tbody>
                </table>
                <!--Table-->
            </div>
            @else
            <p>{{__('form.Form_joueur_aucun')}}</p>
            @endif
        </div>
        <hr> 
        <div class="card">
            <div class="card-header customstats white-text text-center">
                <h1>Section : {{__('form.Form_admin_section_stats')}}</h1>
            </div>
            <div class="card-body">
            @if(count($stats) > 0)
                <div class="table-wrapper-2">
                    <!--Table-->
                    <table class="table table-responsive text-center">
                        <thead class="mdb-color lighten-4">
                            <tr>
                                <th>#</th>
                                <th class="th-lg">{{__('form.Form__equipe_nom')}}</th>
                                <th class="th-lg">{{__('form.Form_Nom')}}</th>
                                <th class="th-lg">{{__('form.Form_annee')}}</th>
                                <th class="th-lg">{{__('form.Form_Creer')}}</th>
                                <th class="th-lg">{{__('form.Form_Misjour')}}</th>
                                <th class="th-lg"></th>
                                <th class="th-lg"></th>
                                <th class="th-lg"></th>
                                <th class="th-lg"></th>
                            </tr>
                        </thead>
                        <tbody>
                                @for($cpt = 0; $cpt < count($stats); $cpt++)
                                <tr>
                                    <th scope="row">
                                        <?php
                                            $value = session('id_incre_stats');
                                            $value++;
                                            session(['id_incre_stats' => $value]);
                                            echo(session('id_incre_stats'));
                                        ?>
                                    </th>
                                    <td>{{$array_nom_equipe_stats[$cpt][0]['nom_equipe']}}</td>
                                    <td>{{$array_nom_joueur[$cpt][0]['nom_joueur']}}</td>
                                    <td>{{$stats[$cpt]->annee_stats}}</td>
                                    <td>{{$stats[$cpt]->created_at}}</td>
                                    <td>{{$stats[$cpt]->updated_at}}</td>
                                    <td><form action="/stats/{{$stats[$cpt]->id}}/edit"><button type="submit" id="buttonplayersize" class="btn btn-info btn-sm">{{__('form.Button_Modfier')}}</button></form></td>
                                    <td>
                                        <form action="{{ action('StatsController@destroy', $stats[$cpt]->id) }}" method="post" class="pull-right">
                                            {{ method_field('DELETE') }}
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                            <button id="buttonplayersize" type="sumbit" class="btn btn-danger btn-sm">{{__('form.Button_Supprimer')}}</button>
                                        </form>
                                    </td>
                                </tr>
                                @endfor
                        </tbody>
                    </table>
                    <!--Table-->
                </div>
                @else
                <p>{{__('form.Form_admin_section_stats_aucun')}}</p>
                @endif
            </div>
                <div class="card-footer text-muted customstats white-text text-right">
            </div>
        </div>
    </div>
    </div>
        <div class="card-footer text-muted orange white-text text-right">
    </div>
</div>
@endsection