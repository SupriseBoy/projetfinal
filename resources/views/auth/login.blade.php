@extends('layouts.app')

@section('content')
<div class="card">
    <div class="card-header indigo white-text text-center">
        {{__('form.Navbar_Connexion')}}
    </div>
    <div class="card-body">
        <form class="form-horizontal" method="POST" action="{{ route('login') }}">
        {{ csrf_field() }}
            <div class="md-form">
                <i class="fa fa-envelope prefix"></i>
                <input type="email" name="email" id="email" class="form-control validate" value="{{ old('email') }}">
                <label for="email" data-error="Erreur" data-success="">{{__('form.Form_email')}}</label>
            </div>
            <div class="md-form">
                <i class="fa fa-lock prefix"></i>
                <input type="password" name="password" id="password" class="form-control validate">
                <label for="password" data-error="Erreur" data-success="">{{__('form.Form_motdepasse')}}</label>
            </div>

            <div class="text-right">
                <button type="submit" class="btn btn-indigo btn-md waves-light">{{__('form.Navbar_Connexion')}}</button>
            </div>
            <div class="text-right">
                <div class="checkbox">
                    <label>
                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> {{__('form.Form_souvenir')}}
                    </label>
                </div>
            </div>

        </form>
    </div>
</div>
<div class="card-footer indigo text-center">
    <a class="btn-link white-text" href="{{ route('password.request') }}">{{__('form.Form_motdepasse_oublier')}}</a>
</div>
@endsection
