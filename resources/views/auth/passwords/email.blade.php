@extends('layouts.app')

@section('content')
<div class="card">
    <div class="card-header indigo white-text text-center">
        {{__('form.Form_reset')}}
    </div>
    <div class="card-body">
        <form class="form-horizontal" method="POST" action="{{ route('password.email') }}">
        {{ csrf_field() }}
            <div class="md-form">
                <i class="fa fa-envelope prefix"></i>
                <input type="email" name="email" id="email" class="form-control validate" value="{{ old('email') }}">
                <label for="email" data-error="Erreur" data-success="">{{__('form.Form_email')}}</label>
            </div>
            <div class="text-right">
                <button type="submit" class="btn btn-indigo btn-md waves-light">{{__('form.Form_Nouveau_motdepasse')}}</button>
            </div>
        </form>
    </div>
</div>
<div class="card-footer indigo text-center">
</div>

@endsection
