@extends('layouts.app')

@section('content')
<div class="card">
    <div class="card-header indigo white-text text-center">
        {{__('form.Navbar_Inscription')}}
    </div>
    <div class="card-body">
        <form class="form-horizontal" method="POST" action="{{ route('register') }}">
        {{ csrf_field() }}
            <div class="md-form">
                <i class="fa fa-user prefix"></i>
                <input type="text" name="name" id="name" class="form-control validate" value="{{ old('name') }}" required autofocus>
                <label for="name" data-error="Erreur" data-success="">{{__('form.Form_nom_user')}}</label>
            </div>
            <div class="md-form">
                <i class="fa fa-envelope prefix"></i>
                <input type="email" name="email" id="email" class="form-control validate" value="{{ old('email') }}">
                <label for="email" data-error="Erreur" data-success="">{{__('form.Form_email')}}</label>
            </div>
            <div class="md-form">
                <i class="fa fa-lock prefix"></i>
                <input type="password" name="password" id="password" class="form-control validate">
                <label for="password" data-error="Erreur" data-success="">{{__('form.Form_motdepasse')}}</label>
            </div>
            <div class="md-form">
                <i class="fa fa-lock prefix"></i>
                <input type="password" name="password_confirmation" id="password-confirm" class="form-control validate">
                <label for="password-confirm" data-error="Erreur" data-success="">{{__('form.Form_motdepasse_confir')}}</label>
            </div>
            <div class="text-right">
                <button type="submit" class="btn btn-indigo btn-md waves-light">{{__('form.Navbar_Inscription')}}</button>
            </div>
        </form>
    </div>
</div>
<div class="card-footer indigo text-center">
</div>
@endsection
