@extends('layouts.app') 
@section('content')
<section class="section feature-box white-text text-center">

    <!--Section heading-->
    <h2 class="section-heading pt-4 display-1 mb-1 cyan-text font-bold" style="display: inline">PROJET FINAL </h2>
    <h2 style="display: inline" class=" section-heading pt-4 display-1 mb-1 customcolor-text font-bold">PHP</h2>
    <hr>

    <div class="row">
        <hr><!--Space-->
    </div>

    <!--Grid row-->
    <div class="row">

        <!--Grid column-->
        <div class="col-md-6 mb-r">
            <i class="fa fa-group fa-5x customcolor-text"></i>
            <h5 class="feature-title">{{__('form.Form_Analytics')}}</h5>
            <p class="white-text">{{__('form.Form_Analytics_description')}}</p>
            <a class="btn btn-success" href="/home" id="buttonsizeHome_page">{{__('form.Button_Analytics')}}</a>
        </div>
        <!--Grid column-->
        <hr>
        <!--Grid column-->
        <div class="col-md-6 mb-r">
            <i class="fa fa-comments fa-5x cyan-text"></i>
            <h5 class="feature-title">{{__('form.Form_Blog')}}</h5>
            <p class="white-text">{{__('form.Form_Blog_description')}}</p>
            <a class="btn btn-cyan disabled" href="" id="buttonsizeHome_page" >{{__('form.Button_Blog')}}</a>
        </div>
        <!--Grid column-->

    </div>
    <!--Grid row-->
</section>
@endsection