 @if(count($errors) > 0)
<div class="panel panel-danger">
    <div class="panel-heading">
        <h3 class="panel-title">List of error</h3>
    </div>
    <div class="panel-body">
        <div class="alert alert-danger">
            @foreach($errors->all() as $error)
                <h4 class="text-justify">{{$error}}</h4>
            @endforeach
        </div>
    </div>
</div>
 @endif

@if(session('success'))
    <div class="alert alert-success">
        {{session('success')}}
    </div>  
 @endif

 @if(session('error'))
    <div class="alert alert-danger">
        {{session('error')}}
    </div>  
 @endif
