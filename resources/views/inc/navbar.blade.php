<nav class="navbar fixed-top navbar-expand-lg navbar-dark scrolling-navbar">
    <logo>
        <a class="navbar-brand" href="/">
            <img src="https://mdbootstrap.com/img/logo/mdb-transparent.png" height="30" class="d-inline-block align-top" alt=""> 
            Bootstrap
        </a>
    </logo>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a class="nav-link" href="/">{{__('form.Navbar_Accueil')}}</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/home">{{__('form.Form_Analytics')}}</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/joueur">{{__('form.Form_admin_section_joueur')}}</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/stats">{{__('form.Form_admin_section_stats')}}</a>
            </li>
        </ul>

        <ul class="nav navbar-nav navbar-right">
                    <!-- Authentication Links -->
                        @guest
                        <li class="nav-item"><a class="nav-link" href="{{ route('login')}} ">{{__('form.Navbar_Connexion')}}</a></li>
                        <li class="nav-item"><a class="nav-link" href="{{ route('register')}} ">{{__('form.Navbar_Inscription')}}</a></li>
                    @else
                        <li class="nav-item dropdown">
                            <a class="nav-link" href="" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true">
                                {{ Auth::user()->name }} <span class="caret"></span>
                            </a>

                            <ul class="dropdown-menu">
                                <li class="dropdown-item waves-light">
                                    <a class="nav-link" href="route('logout')"
                                        onclick="event.preventDefault();
                                                    document.getElementById('logout-form').submit();">
                                        {{ __('form.Navbar_Deconnexion') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                </li>
                                @if(\Auth::user()->admin == 1)
                                    <li class="dropdown-item waves-light">
                                        <a class="nav-link" href="/admin">
                                            {{__('form.Navbar_Admin')}}
                                        </a>
                                    </li>
                                @endif
                            </ul>
                        </li>
                    @endguest
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle waves-light" href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Langue<span class="caret"></span></a>
                                        
                            <div dropdownMenu class="dropdown-menu dropdown dropdown-primary" role="menu">
                                <a class="nav-link dropdown-item waves-light" mdbRippleRadius href="/lang/fr">FR</a>
                                <a class="nav-link dropdown-item waves-light" mdbRippleRadius href="/lang/en">EN</a>
                            </div>
                    </li>
                </ul>                   
    </div>
</nav>
