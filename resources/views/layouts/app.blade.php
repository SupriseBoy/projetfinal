<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ config('app.name', 'Laravel') }}</title>

        <!-- Font Awesome -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

        <!-- Styles -->
        <link rel="stylesheet" href="{{ asset('css/bootstrap/css/bootstrap.css') }}">
        <link rel="stylesheet" href="{{ asset('css/bootstrap/css/mdb.css') }}">
        <link rel="stylesheet" href="{{ asset('css/bootstrap/css/style.css') }}">

        <!-- Data tables -->
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap4.min.css"/>

    </head>
    <body>
        <header>
            @include('inc.navbar')
        </header>
        <main class="pt-md-10-custom">
            <div class="container">
                @include('inc.messages') 
                @yield('content')
            </div>
        </main>
        <footer class="page-footer center-on-small-only unique-color-dark" >
            <div class="footer-copyright">
                <div class="container-fluid">
                    © 2017 Copyright: <a href="https://www.supriseboy.alwaysdata.net"><strong> MDBootstrap.com</strong></a>
                </div>
            </div>   
        </footer>  
               
        <script src="{{ asset('css/bootstrap/js/jquery-3.2.1.min.js') }}"></script>
        <script src="{{ asset('css/bootstrap/js/popper.min.js') }}"></script>
        <script src="{{ asset('css/bootstrap/js/bootstrap.js') }}"></script>
        <script src="{{ asset('css/bootstrap/js/mdb.js') }}"></script>
        <script src="{{ asset('css/bootstrap/js/dataTables.js') }}"></script>
        <script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap4.min.js"></script>
        <script src="{{ asset('css/bootstrap/js/custom.js') }}"></script>
        <script src="{{ asset('css/bootstrap/js/piechart.js') }}"></script>
    </body> 
</html>
