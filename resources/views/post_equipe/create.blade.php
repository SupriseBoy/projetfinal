@extends('layouts.app') 
@section('content')
<div class="card">
    <div class="card-header customcolor white-text text-center">
        <div class="row">
            <div class="col-md-1">
                <a href="/equipe">
                    <i class="fa fa-arrow-circle-left fa-3x"></i>
                </a>
            </div>
            <div class="col-md-10">
                <h2 class="font-bold">{{__('form.Form_Liste_equipe')}}</h2>
            </div>
        </div>
    </div>
    <div class="card-body">
        <form class="form-horizontal" method="POST" action="{{ action('EquipeController@store') }}" enctype="multipart/form-data">
        {{ csrf_field() }}
            <div class="md-form">
                <i class="fa fa-envelope prefix"></i>
                <input type="text" name="nom_equipe" id="nom_equipe" class="form-control validate" value="{{ old('nom_equipe') }}">
                <label for="nom_equipe" data-error="Erreur" data-success="">{{__('form.Form__equipe_nom')}}</label>
            </div>
            <div class="md-form">
                <i class="fa fa-pencil prefix"></i>
                <textarea name="description_equipe" type="text" id="description_equipe" class="md-textarea validate">{{ old('description_equipe') }}</textarea>
                <label for="description_equipe">{{__('form.Form__equipe_description')}}</label>
            </div>
            <div class="sm-form text-center">
                <div class="row">
                    <div class="col-md-12">
                        <div class="justify-content-md-center">
                            <i class="fa fa-image" aria-hidden="true"></i>
                            <label>{{__('form.Form__equipe_image')}}</label>
                        </div>
                        <div class="justify-content-md-center">
                            <input class="btn btn-success" type="file" name="cover_image">
                        </div>
                    </div>
                </div>
            </div>
            <hr>
            <button type="submit" class="btn btn-success btn-md waves-light btn-block">{{__('form.Button_ajouter')}}</button>
        </form>
    </div>
</div>
<div class="card-footer customcolor text-center">
</div>  
@endsection