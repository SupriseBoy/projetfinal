@extends('layouts.app') 
@section('content')

<section class="section feature-box">

    <!--Section heading-->
    <h1 class="section-heading pt-2 display-3 mb-5 white-text text-center">{{__('form.Form_home_description_titre')}}</h1>
    

    <!--Grid row-->
    <div class="row">

        <!--Grid column-->
        <div class="col-lg-4 text-left">
            <img src="/storage/images/front_end/logo.png" alt="" class="img-fluid z-depth-0">
        </div>
        <!--Grid column-->

        <!--Grid column-->
        <div class="col-lg-8">

            <!--Grid row-->
            <div class="row pb-2">
                <div class="col-2 col-md-1">
                    <i class="fa fa-group customcolor-text fa-2x"></i>
                </div>
                <div class="col-10 text-right">
                    <h5 class="white-text text-left font-bold">{{__('form.Form_admin_section_equipe')}}</h5>
                    <p class="white-text text-justify">{{__('form.Form_admin_section_equipe_desc')}}</p>
                    <a class="btn btn-success btn-sm" href="/equipe" id="buttonsizeHome_page">{{__('form.Form_admin_section_equipe_button')}}</a>
                </div>
            </div>
            <!--Grid row-->

            <!--Grid row-->
            <div class="row pb-2">
                <div class="col-2 col-md-1">
                    <i class="fa fa-user-plus customcolorplayer-text fa-2x"></i>
                </div>
                <div class="col-10 text-right">
                    <h5 class="white-text text-left font-bold">{{__('form.Form_admin_section_joueur')}}</h5>
                    <p class="white-text text-justify">{{__('form.Form_admin_section_joueur_desc')}}.</p>
                    <a class="btn btn-customplayer btn-sm" href="/joueur" id="buttonsizeHome_page">{{__('form.Form_admin_section_joueur_button')}}</a>
                </div>
            </div>
            <!--Grid row-->

            <!--Grid row-->
            <div class="row">
                <div class="col-2 col-md-1">
                    <i class="fa fa-bar-chart yellow-text fa-2x"></i>
                </div>
                <div class="col-10 text-right">
                    <h5 class="white-text text-left font-bold">{{__('form.Form_admin_section_stats')}}</h5>
                    <p class="white-text text-justify">{{__('form.Form_admin_section_stats_desc')}}</p>
                    <a class="btn btn-customstats btn-sm" href="/stats" id="buttonsizeHome_page">{{__('form.Form_admin_section_stats_button')}}</a>
                </div>
            </div>
            <!--Grid row-->

        </div>
        <!--Grid column-->

    </div>
    <!--Grid row-->

</section>
@endsection