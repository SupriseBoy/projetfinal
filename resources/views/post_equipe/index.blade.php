@extends('layouts.app') 
@section('content')
@if(count($equipes) > 0)
    @foreach ($equipes as $equipe)
        <div class="card">
            <a id="custom_navlink" class="nav link" href="/equipe/{{$equipe->id_equipe}}">
                <div class="card-header success-color white-text">
                    {{$equipe->nom_equipe}}
                </div>
            </a>
            <div class="card-body">
                <div class="row">
                    <div class="col-4">
                        <img src="/storage/images/equipe_images/{{$equipe->image_equipe}}" alt="" class="img-fluid z-depth-3">
                    </div>
                    <div class="col-8">
                        <h4 class="card-title">{{__('form.Form__equipe_description')}}</h4>
                        <hr>
                        <p class="card-text black-text text-justify">{{$equipe->description_equipe}}</p>
                        @if(!Auth::guest())
                            @if(Auth::user()->id == $equipe->id_user)
                                <div class="row">
                                    <div class="col-md-7">
                                    </div>
                                    <div class="col-md-2">
                                        <a id="buttonsizequipe" class="btn btn-info btn-sm" href="/equipe/{{$equipe->id_equipe}}/edit">{{__('form.Button_Modfier')}}</a>   
                                    </div>
                                    <div class="col-md-3">
                                        <form action="{{ action('EquipeController@destroy', $equipe->id_equipe) }}" method="post" class="pull-right">
                                            {{ method_field('DELETE') }}
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                            <a href="/equipe/{{$equipe->id_equipe}}">
                                                <button id="buttonsizequipe" type="sumbit" class="btn btn-danger btn-sm">{{__('form.Button_Supprimer')}}</button>
                                            </a>
                                        </form>   
                                    </div>
                                </div>
                            @endif
                        @endif
                    </div>
                </div>
            </div>
            <div class="card-footer text-muted success-color white-text text-right">
                <p class="mb-0"><?php echo(date("d-m-Y", strtotime($equipe->created_at))); ?></p>
            </div>
        </div>
        <hr>
    @endforeach
    {{$equipes->links()}}
    @if(!Auth::guest())
    <a class="btn btn-success btn-block" href="/equipe/create">{{__('form.Button_Ajouter_equipe')}}</a>
    @endif

@else
<p class="white-text text-center">{{__('form.Form_equipe_aucun')}}</p>
@if(!Auth::guest())
    <a class="btn btn-success btn-block" href="/equipe/create">{{__('form.Button_Ajouter_equipe')}}</a>
@endif
@endif
@endsection