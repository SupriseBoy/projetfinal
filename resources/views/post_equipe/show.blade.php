@extends('layouts.app') 
@section('content')
<div class="card">
    <div class="card-header customcolor white-text text-center">
        <div class="row">
            <div class="col-md-1">
                <a href="/equipe">
                    <i class="fa fa-arrow-circle-left fa-3x"></i>
                </a>
            </div>
            <div class="col-md-10">
                <div class="text-center">
                    <h2 class="font-bold">{{$equipe->nom_equipe}}</h2>
                </div>
            </div>
        </div>
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col-4">
                <div class="view overlay hm-white-light flex-center">
                    <img src="/storage/images/equipe_images/{{$equipe->image_equipe}}" alt="" class="img-fluid z-depth-3" style="width:325px;height:225px;">
                    <div class="mask flex-center waves-effect waves-light">
                        <h2 class="grey-text font-bold">{{__('form.Form_image_pro')}}</h2>
                    </div>
                </div>
            </div>
            <div class="col-8">
                <h4 class="card-title text-center">{{__('form.Form__equipe_description')}}</h4>
                <hr>
                <p class="card-text black-text text-justify">{{$equipe->description_equipe}}</p>
            </div>
        </div>

        <div class="col-md-12">
            <hr>
            <div class="card">
                <div class="card-header black-text text-center">
                    <h2 class="font-bold">{{__('form.Form_Liste_joueurs')}}</h2>
                </div>
                <div class="card-body">
                    @if(count($joueurs) > 0)
                    <div class="table-wrapper-2">
                        <!--Table-->
                        <table class="table table-responsive text-center">
                            <thead class="mdb-color lighten-4">
                                <tr>
                                    <th>#</th>
                                    <th class="th-lg">{{__('form.Form_Nom')}}</th>
                                    <th class="th-lg">{{__('form.Form_Creer')}}</th>
                                    <th class="th-lg">{{__('form.Form_Misjour')}}</th>
                                    <th class="th-lg"></th>
                                    <th class="th-lg"></th>
                                    <th class="th-lg"></th>
                                    <th class="th-lg"></th>
                                </tr>
                            </thead>
                            <tbody>
                                    @foreach($joueurs as $joueur)
                                    <tr>
                                        <th scope="row">
                                            <?php
                                                $value = session('id_incre');
                                                $value++;
                                                session(['id_incre' => $value]);
                                                echo(session('id_incre'));
                                            ?>
                                        </th>
                                        <td>{{$joueur->nom_joueur}}</td>
                                        <td>{{$joueur->created_at}}</td>
                                        <td>{{$joueur->updated_at}}</td>
                                        <td><form action="/joueur/{{$joueur->id_joueur}}"><button id="buttonplayersize" class="btn btn-purple btn-sm">Info</button></form></td>
                                        @if(!Auth::guest())
                                            @if(Auth::user()->id == $joueur->id_user)
                                                <td><form action="/joueur/{{$joueur->id_joueur}}/edit"><button id="buttonplayersize" class="btn btn-info btn-sm">{{__('form.Button_Modfier')}}</button></form></td>
                                                <td>
                                                <form action="{{ action('JoueurController@destroy', $joueur->id_joueur) }}" method="post" class="pull-right">
                                                        {{ method_field('DELETE') }}
                                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                        <button id="buttonplayersize" type="sumbit" class="btn btn-danger btn-sm">{{__('form.Button_Supprimer')}}</button>
                                                    </form>
                                                </td>
                                            @endif
                                        @endif
                                    </tr>
                                    @endforeach
                            </tbody>
                        </table>
                        <!--Table-->
                    </div>
                    @else
                        <div class="text-center">
                            <p>{{__('form.Form_joueur_aucun')}}</p>
                        </div>
                    @endif
                </div>
            </div>
        </div>
        <hr>
        @if(!Auth::guest())
        <a class="btn btn-purple btn-block" href="/joueur/create">{{__('form.Button_Ajouter_joueur')}}<a/> 
        @endif
    </div>
</div>
@endsection