@extends('layouts.app') 
@section('content')
<div class="card">
    <div class="card-header purple white-text">
        <div class="row">
            <div class="col-md-5">
                <a href="/equipe/{{$joueur->id_equipe}}">
                    <i class="fa fa-arrow-circle-left fa-2x"></i>
                </a>
            </div>
            <div class="col-md-7">
                {{$joueur->nom_joueur}}
            </div>
        </div>
    </div>
    <div class="card-body">
        <form class="form-horizontal" method="POST" action="{{ action('JoueurController@update', $joueur->id_joueur) }}" enctype="multipart/form-data">
            {{ method_field('PUT') }}
            <div class="md-form">
                <i class="fa fa-envelope prefix"></i>
                <input type="text" name="nom_joueur" id="nom_joueur" class="form-control validate" value="{{ $joueur->nom_joueur }}">
                <label for="nom_joueur" data-error="Erreur" data-success="">{{__('form.Form_Nom')}}</label>
            </div>
            <div class="md-form">
                <i class="fa fa-money prefix"></i>
                <input type="number" name="salaire_joueur" id="salaire_joueur" class="form-control validate" value="{{ $joueur->salaire_joueur }}">
                <label for="salaire_joueur" data-error="Erreur" data-success="">{{__('form.Form_Salaire')}}</label>
            </div>
            <div class="md-form">
                <i class="fa fa-globe prefix"></i>
                <input type="text" name="pays_joueur" id="pays_joueur" class="form-control validate" value="{{ $joueur->pays_joueur }}">
                <label for="pays_joueur" data-error="Erreur" data-success="">{{__('form.Form_Pays')}}</label>
            </div>
            <div class="md-form">
                <i class="fa fa-pencil prefix"></i>
                <textarea name="description_joueur" type="text" id="description_joueur" class="md-textarea validate">{{ $joueur->description_joueur }}</textarea>
                <label for="description_joueur">{{__('form.Form_Description')}}</label>
            </div>
            <div class="sm-form text-center">
                <div class="row">
                    <div class="col-md-12">
                        <div class="justify-content-md-center">
                            <i class="fa fa-image" aria-hidden="true"></i>
                            <label>{{__('form.Form_joueur_image')}}</label>
                        </div>
                        <div class="justify-content-md-center">
                            <input class="btn btn-purple" type="file" name="cover_image">
                        </div>
                            <img src="/storage/images/joueur_images/{{$joueur->image_joueur}}" alt="" class="img-fluid z-depth-3 rounded-circle">
                    </div>
                </div>
            </div>
            <hr>
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <button type="submit" class="btn btn-purple btn-md waves-light btn-block">{{__('form.Button_Modfier_')}}</button>
        </form>
    </div>
</div>
<div class="card-footer purple text-center">
</div>  
@endsection