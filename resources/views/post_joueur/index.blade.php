@extends('layouts.app') 
@section('content')
<div class="card">
    <div class="card-header purple white-text text-center">
        <h3 class="font-bold">{{__('form.Form_Liste_joueurs')}}</h3>
    </div>
    <div class="card-body">
            <table id="data_search" class="table table-striped table-bordered table-responsive" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>{{__('form.Form_Nom')}}</th>
                    <th>{{__('form.Form_Salaire')}}</th>
                    <th>{{__('form.Form_Pays')}}</th>
                    <th>{{__('form.Form__equipe_nom')}}</th>
                    <th>{{__('form.Form_Creer')}}</th>
                    <th>{{__('form.Form_Misjour')}}</th>
                </tr>
            </thead>
            <tbody>
                @if(count($joueurs) > 0)
                    @for($cpt = 0; $cpt < count($joueurs); $cpt++)

                        <tr>
                            <th>{{$joueurs[$cpt]->nom_joueur}}</th>
                            <th>{{$joueurs[$cpt]->salaire_joueur}}</th>
                            <th>{{$joueurs[$cpt]->pays_joueur}}</th>
                            <th>{{$array_nom_equipes[$cpt][0]['nom_equipe']}}</th>
                            <th>{{$joueurs[$cpt]->created_at}}</th>
                            <th>{{$joueurs[$cpt]->updated_at}}</th>
                        </tr>

                    @endfor
                @else
                <p>{{__('form.Form_joueur_aucun')}}</p>
                @endif
            </tbody>
        </table>
    </div>
</div>
<div class="card-footer purple text-center">
</div>
@endsection