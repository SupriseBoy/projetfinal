@extends('layouts.app') 
@section('content')
<div class="card">
    <div class="card-header purple white-text">
        <div class="row">
            <div class="col-md-5">
                <a href="/equipe/{{$joueur->id_equipe}}">
                    <i class="fa fa-arrow-circle-left fa-2x"></i>
                </a>
            </div>
            <div class="col-md-7">
                <h3 class="font-bold">{{$joueur->nom_joueur}}</h3>
            </div>
        </div>
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col-3">
                <div class="view overlay hm-white-light flex-center">
                    <img src="/storage/images/joueur_images/{{$joueur->image_joueur}}" alt="" class="img-fluid z-depth-3">
                    <div class="mask flex-center waves-effect waves-light">
                        <h2 class="grey-text font-bold">{{__('form.Form_image_pro')}}</h2>
                    </div>
                </div>  
            </div>
            <div class="col-9">
                <h4 class="card-title">{{__('form.Form_Description')}}</h4>
                <hr>
                <p class="card-text black-text text-justify">{{$joueur->description_joueur}}</p>
            </div>
        </div>
        <hr>
<div class="card">
    <div class="card-header purple white-text">
        @if(count($data_stats) > 0)
        <div class="row">
            <div class="col-4 text-center">
                <h4 class="font-bold">{{__('form.Form_Stats_desc_1')}} {{$data_stats[0]['annee_stats']}}</h4>
            </div>
            <div class="col-8 text-center">
                <h4 class="font-bold">{{__('form.Form_Stats_desc_2_1')}} {{count($data_stats_chart_bar)}} {{__('form.Form_Stats_desc_2_2')}}</h4>
            </div>
        </div>
        @else
            <div class="text-center">
                <p>{{__('form.Form_stats_aucun')}}</p>
            </div>
        @endif
    </div>
    <div class="card-body">
    @if(count($data_stats) > 0)
    <div class="row">
        <div class="col-4">
            <canvas id="pieChart" style="height:200px;width: content-box;">
                <input type="hidden" id="pieChart_goal" value="{{$data_stats[0]['nbre_points']}}" />
                <input type="hidden" id="pieChart_assistes" value="{{$data_stats[0]['nbre_assistes']}}" />
            </canvas>
        </div>
            <div class="col-4">
                <canvas id="barChartPoint">
                    @switch(count($data_stats_chart_bar))
                        @case(1)
                            @if(count($data_stats_chart_bar) == 1)
                                <input type="hidden" id="barChartPoint_0" value="{{$data_stats_chart_bar[0]['nbre_points']}}" />
                                <input type="hidden" id="barChartAnnee_0" value="{{$data_stats_chart_bar[0]['annee_stats']}}" />
                            @endif
                        @case(2)
                            @if(count($data_stats_chart_bar) == 2)
                                <input type="hidden" id="barChartPoint_0" value="{{$data_stats_chart_bar[0]['nbre_points']}}" />
                                <input type="hidden" id="barChartAnnee_0" value="{{$data_stats_chart_bar[0]['annee_stats']}}" />
                                <input type="hidden" id="barChartPoint_1" value="{{$data_stats_chart_bar[1]['nbre_points']}}" />
                                <input type="hidden" id="barChartAnnee_1" value="{{$data_stats_chart_bar[1]['annee_stats']}}" />
                            @endif   
                        @case(3)
                            @if(count($data_stats_chart_bar) == 3)
                                <input type="hidden" id="barChartPoint_0" value="{{$data_stats_chart_bar[0]['nbre_points']}}" />
                                <input type="hidden" id="barChartAnnee_0" value="{{$data_stats_chart_bar[0]['annee_stats']}}" />
                                <input type="hidden" id="barChartPoint_1" value="{{$data_stats_chart_bar[1]['nbre_points']}}" />
                                <input type="hidden" id="barChartAnnee_1" value="{{$data_stats_chart_bar[1]['annee_stats']}}" />
                                <input type="hidden" id="barChartPoint_2" value="{{$data_stats_chart_bar[2]['nbre_points']}}" />
                                <input type="hidden" id="barChartAnnee_2" value="{{$data_stats_chart_bar[2]['annee_stats']}}" />
                            @endif   
                        @case(4)
                            @if(count($data_stats_chart_bar) == 4)
                                <input type="hidden" id="barChartPoint_0" value="{{$data_stats_chart_bar[0]['nbre_points']}}" />
                                <input type="hidden" id="barChartAnnee_0" value="{{$data_stats_chart_bar[0]['annee_stats']}}" />
                                <input type="hidden" id="barChartPoint_1" value="{{$data_stats_chart_bar[1]['nbre_points']}}" />
                                <input type="hidden" id="barChartAnnee_1" value="{{$data_stats_chart_bar[1]['annee_stats']}}" />
                                <input type="hidden" id="barChartPoint_2" value="{{$data_stats_chart_bar[2]['nbre_points']}}" />
                                <input type="hidden" id="barChartAnnee_2" value="{{$data_stats_chart_bar[2]['annee_stats']}}" />
                                <input type="hidden" id="barChartPoint_3" value="{{$data_stats_chart_bar[3]['nbre_points']}}" />
                                <input type="hidden" id="barChartAnnee_3" value="{{$data_stats_chart_bar[3]['annee_stats']}}" />
                            @endif   
                        @case(5)
                            @if(count($data_stats_chart_bar) == 5)
                                <input type="hidden" id="barChartPoint_0" value="{{$data_stats_chart_bar[0]['nbre_points']}}" />
                                <input type="hidden" id="barChartAnnee_0" value="{{$data_stats_chart_bar[0]['annee_stats']}}" />
                                <input type="hidden" id="barChartPoint_1" value="{{$data_stats_chart_bar[1]['nbre_points']}}" />
                                <input type="hidden" id="barChartAnnee_1" value="{{$data_stats_chart_bar[1]['annee_stats']}}" />
                                <input type="hidden" id="barChartPoint_2" value="{{$data_stats_chart_bar[2]['nbre_points']}}" />
                                <input type="hidden" id="barChartAnnee_2" value="{{$data_stats_chart_bar[2]['annee_stats']}}" />
                                <input type="hidden" id="barChartPoint_3" value="{{$data_stats_chart_bar[3]['nbre_points']}}" />
                                <input type="hidden" id="barChartAnnee_3" value="{{$data_stats_chart_bar[3]['annee_stats']}}" />
                                <input type="hidden" id="barChartPoint_4" value="{{$data_stats_chart_bar[4]['nbre_points']}}" />
                                <input type="hidden" id="barChartAnnee_4" value="{{$data_stats_chart_bar[4]['annee_stats']}}" />
                            @endif
                    @endswitch
                </canvas>
        </div>
            <div class="col-4">
                <canvas id="barChartShot">
                @switch(count($data_stats_chart_bar))
                        @case(1)
                            @if(count($data_stats_chart_bar) == 1)
                                <input type="hidden" id="barChartShot_0" value="{{$data_stats_chart_bar[0]['nbre_tir']}}" />
                                <input type="hidden" id="barChartAnnee_0" value="{{$data_stats_chart_bar[0]['annee_stats']}}" />
                            @endif
                        @case(2)
                            @if(count($data_stats_chart_bar) == 2)
                                <input type="hidden" id="barChartShot_0" value="{{$data_stats_chart_bar[0]['nbre_tir']}}" />
                                <input type="hidden" id="barChartAnnee_0" value="{{$data_stats_chart_bar[0]['annee_stats']}}" />
                                <input type="hidden" id="barChartShot_1" value="{{$data_stats_chart_bar[1]['nbre_tir']}}" />
                                <input type="hidden" id="barChartAnnee_1" value="{{$data_stats_chart_bar[1]['annee_stats']}}" />
                            @endif   
                        @case(3)
                            @if(count($data_stats_chart_bar) == 3)
                                <input type="hidden" id="barChartShot_0" value="{{$data_stats_chart_bar[0]['nbre_tir']}}" />
                                <input type="hidden" id="barChartAnnee_0" value="{{$data_stats_chart_bar[0]['annee_stats']}}" />
                                <input type="hidden" id="barChartShot_1" value="{{$data_stats_chart_bar[1]['nbre_tir']}}" />
                                <input type="hidden" id="barChartAnnee_1" value="{{$data_stats_chart_bar[1]['annee_stats']}}" />
                                <input type="hidden" id="barChartShot_2" value="{{$data_stats_chart_bar[2]['nbre_tir']}}" />
                                <input type="hidden" id="barChartAnnee_2" value="{{$data_stats_chart_bar[2]['annee_stats']}}" />
                            @endif   
                        @case(4)
                            @if(count($data_stats_chart_bar) == 4)
                                <input type="hidden" id="barChartShot_0" value="{{$data_stats_chart_bar[0]['nbre_tir']}}" />
                                <input type="hidden" id="barChartAnnee_0" value="{{$data_stats_chart_bar[0]['annee_stats']}}" />
                                <input type="hidden" id="barChartShot_1" value="{{$data_stats_chart_bar[1]['nbre_tir']}}" />
                                <input type="hidden" id="barChartAnnee_1" value="{{$data_stats_chart_bar[1]['annee_stats']}}" />
                                <input type="hidden" id="barChartShot_2" value="{{$data_stats_chart_bar[2]['nbre_tir']}}" />
                                <input type="hidden" id="barChartAnnee_2" value="{{$data_stats_chart_bar[2]['annee_stats']}}" />
                                <input type="hidden" id="barChartShot_3" value="{{$data_stats_chart_bar[3]['nbre_tir']}}" />
                                <input type="hidden" id="barChartAnnee_3" value="{{$data_stats_chart_bar[3]['annee_stats']}}" />
                            @endif   
                        @case(5)
                            @if(count($data_stats_chart_bar) == 5)
                                <input type="hidden" id="barChartShot_0" value="{{$data_stats_chart_bar[0]['nbre_tir']}}" />
                                <input type="hidden" id="barChartAnnee_0" value="{{$data_stats_chart_bar[0]['annee_stats']}}" />
                                <input type="hidden" id="barChartShot_1" value="{{$data_stats_chart_bar[1]['nbre_tir']}}" />
                                <input type="hidden" id="barChartAnnee_1" value="{{$data_stats_chart_bar[1]['annee_stats']}}" />
                                <input type="hidden" id="barChartShot_2" value="{{$data_stats_chart_bar[2]['nbre_tir']}}" />
                                <input type="hidden" id="barChartAnnee_2" value="{{$data_stats_chart_bar[2]['annee_stats']}}" />
                                <input type="hidden" id="barChartShot_3" value="{{$data_stats_chart_bar[3]['nbre_tir']}}" />
                                <input type="hidden" id="barChartAnnee_3" value="{{$data_stats_chart_bar[3]['annee_stats']}}" />
                                <input type="hidden" id="barChartShot_4" value="{{$data_stats_chart_bar[4]['nbre_tir']}}" />
                                <input type="hidden" id="barChartAnnee_4" value="{{$data_stats_chart_bar[4]['annee_stats']}}" />
                            @endif
                        
                    @endswitch
                </canvas>
        </div>
        @else
            <div class="text-center">
                <p>{{__('form.Form_stats_aucun')}}</p>
            </div>
        @endif
    </div>
    <hr>
    <div class="card-header purple white-text">
        <div class="text-center">
            <h4 class="font-bold">{{__('form.Form_stats_list')}}</h4>
        </div>
    </div>
    <div class="card-body">
        @if(count($data_stats_joueur) > 0)
        <div class="table-wrapper-2">
            <!--Table-->
            <table class="table table-responsive text-center">
                <thead class="mdb-color lighten-4">
                    <tr>
                        <th>#</th>
                        <th class="th-lg">{{__('form.Form_annee')}}</th>
                        <th class="th-lg">{{__('form.Form_Creer')}}</th>
                        <th class="th-lg">{{__('form.Form_Misjour')}}</th>
                        <th class="th-lg"></th>
                        <th class="th-lg"></th>
                        <th class="th-lg"></th>
                        <th class="th-lg"></th>
                    </tr>
                </thead>
                <tbody>
                        @foreach($data_stats_joueur as $stats)
                        <tr>
                            <th scope="row">
                                <?php
                                    $value = session('id_incre');
                                    $value++;
                                    session(['id_incre' => $value]);
                                    echo(session('id_incre'));
                                ?>
                            </th>
                            <td>{{$stats->annee_stats}}</td>
                            <td>{{$stats->created_at}}</td>
                            <td>{{$stats->updated_at}}</td>
                            @if(!Auth::guest())
                                @if(Auth::user()->id == $stats->id_user)
                                    <td><form action="/stats/{{$stats->id}}/edit"><button type="submit" id="buttonplayersize" class="btn btn-info btn-sm">{{__('form.Button_Modfier')}}</button></form></td>
                                    <td>
                                        <form action="{{ action('StatsController@destroy', $stats->id) }}" method="post" class="pull-right">
                                            {{ method_field('DELETE') }}
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                            <button id="buttonplayersize" type="sumbit" class="btn btn-danger btn-sm">{{__('form.Button_Supprimer')}}</button>
                                        </form>
                                    </td>
                                @endif
                            @endif
                        </tr>
                        @endforeach
                </tbody>
            </table>
            <!--Table-->
        </div>
        @else
            <div class="text-center">
                <p>{{__('form.Form_admin_section_stats_aucun')}}</p>
            </div>
        @endif
    </div>
    <hr>
    @if(!Auth::guest())
        @if(Auth::user()->id == $joueur->id_user)
            <a href ="/stats/create" class="btn btn-customstats btn-block">{{__('form.Form_stats_button')}}</a> 
        @endif
    @endif
    </div>
</div>
    </div>
    <div class="card-footer text-muted purple white-text text-right">
    </div>
</div>

@endsection