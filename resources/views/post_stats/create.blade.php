@extends('layouts.app') 
@section('content')
<div class="card">
    <div class="card-header customstats white-text text-center">
        <div class="row">
            <div class="col-md-1">
                <a href="/joueur/{{session('id_joueur')}}">
                    <i class="fa fa-arrow-circle-left fa-2x"></i>
                </a>
            </div>
            <div class="col-md-10">
                <h4 class="font-bold">{{__('form.Form_stats_titre')}}</h4>
            </div>
        </div>
    </div>
    <div class="card-body">
        <form class="form-horizontal" method="POST" action="{{ action('StatsController@store') }}">
        {{ csrf_field() }}
            <div class="md-form">
                <i class="fa fa-hashtag prefix" prefix></i>
                <input type="number" name="nbre_partie" id="nbre_partie" class="form-control validate" value="{{ old('nbre_partie') }}">
                <label for="nbre_partie" data-error="Erreur" data-success="">{{__('form.Form_stats_game')}}</label>
            </div>
            <div class="md-form">
                <i class="fa fa-area-chart prefix"></i>
                <input type="number" name="nbre_buts" id="nbre_buts" class="form-control validate" value="{{ old('nbre_buts') }}">
                <label for="nbre_buts" data-error="Erreur" data-success="">{{__('form.Form_stats_but')}}</label>
            </div>
            <div class="md-form">
                <i class="fa fa-line-chart prefix"></i>
                <input type="number" name="nbre_assistes" id="nbre_assistes" class="form-control validate" value="{{ old('nbre_assistes') }}">
                <label for="nbre_assistes" data-error="Erreur" data-success="">{{__('form.Form_stats_assite')}}</label>
            </div>
            <div class="md-form">
                <i class="fa fa-dot-circle-o prefix" aria-hidden="true"></i>
                <input type="number" name="nbre_tir" id="nbre_tir" class="form-control validate" value="{{ old('nbre_tir') }}">
                <label for="nbre_tir" data-error="Erreur" data-success="">{{__('form.Form_stats_tir')}}</label>
            </div>
            <div class="md-form">
                <i class="fa fa-calendar prefix" aria-hidden="true"></i>
                <input type="text" name="annee_stats" id="annee_stats" class="form-control validate" value="{{ old('annee_stats') }}">
                <label for="annee_stats" data-error="Erreur" data-success="">{{__('form.Form_stats_annee')}}</label>
            </div>
            <hr>
            <button type="submit" class="btn btn-customstats btn-md waves-light btn-block">{{__('form.Button_ajouter')}}</button>
        </form>
    </div>
</div>
<div class="card-footer customstats text-center">
</div>  
@endsection