@extends('layouts.app') 
@section('content')
<div class="card">
    <div class="card-header customstats white-text text-center">
        <h3 class="font-bold">{{__('form.Form_stats_list')}}</h3>
    </div>
    <div class="card-body">
            <table id="data_search" class="table table-striped table-bordered table-responsive" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>{{__('form.Form_stats_list')}}</th>
                    <th>{{__('form.Form_stats_game')}}</th>
                    <th>{{__('form.Form_stats_but')}}</th>
                    <th>{{__('form.Form_stats_assite')}}</th>
                    <th>{{__('form.Form_stats_list')}}</th>
                    <th>{{__('form.Form_stats_tir')}}</th>
                    <th>{{__('form.Form_stats_annee')}}</th>
                </tr>
            </thead>
            <tbody>
                @if(count($stats) > 0)
                    @for($cpt = 0; $cpt < count($stats); $cpt++)
                        <tr>
                            <th>{{$array_nom_joueur[$cpt][0]['nom_joueur']}}</th>
                            <th>{{$stats[$cpt]->nbre_partie}}</th>
                            <th>{{$stats[$cpt]->nbre_buts}}</th>
                            <th>{{$stats[$cpt]->nbre_assistes}}</th>
                            <th>{{$stats[$cpt]->nbre_points}}</th>
                            <th>{{$stats[$cpt]->nbre_tir}}</th>
                            <th>{{$stats[$cpt]->annee_stats}}</th>
                        </tr>
                    @endfor
                @else
                <p>{{__('form.Form_admin_section_stats_aucun')}}</p>
                @endif
            </tbody>
        </table>
    </div>
</div>
<div class="card-footer customstats text-center">
</div>
@endsection