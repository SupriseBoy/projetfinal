<ul class="pagination">
<!-- Previous Page Link -->
@if ($paginator->onFirstPage())
    <li class="page-item disabled" style="border-style: solid; display: table; border-color: #a6a6a6; border-width: thin;">
      <a class="page-link" href="{{ $paginator->previousPageUrl() }}" aria-label="Previous" style="vertical-align: middle; display: table-cell;">
        <span aria-hidden="true"><b>&laquo;</b></span>
        <span class="sr-only"><b>Previous</b></span>
      </a>
    </li>
@else
    <li class="page-item" style="border-style: solid; display: table; border-color: #a6a6a6; border-width: thin;">
      <a class="page-link" href="{{ $paginator->previousPageUrl() }}" aria-label="Previous" style="vertical-align: middle; display: table-cell;">
        <span aria-hidden="true"><b>&laquo;</b></span>
        <span class="sr-only"><b>Previous</b></span>
      </a>
    </li>
@endif

<!-- Pagination Elements -->
@foreach ($elements as $element)
    <!-- "Three Dots" Separator -->
    @if (is_string($element))
        <li class="disabled"><span>{{ $element }}</span></li>
    @endif

    <!-- Array Of Links -->
    @if (is_array($element))
        @foreach ($element as $page => $url)
            @if ($page == $paginator->currentPage())
            <li class="page-item active" style="border-style: solid; display: table; border-color: #a6a6a6; border-width: thin;">
                <a class="page-link" href="#" style="vertical-align: middle; display: table-cell;"><b>{{ $page }}</b></a>
            </li>
            @else
            <li class="page-item" style="border-style: solid; display: table; border-color: #a6a6a6; border-width: thin;">
                <a class="page-link" href="{{ $url }}" style="vertical-align: middle; display: table-cell;"><b>{{ $page }}</b></a>
            </li>

            @endif
        @endforeach
    @endif
@endforeach

<!-- Next Page Link -->
@if ($paginator->hasMorePages())
    <li class="page-item" style="border-style: solid; display: table; border-color: #a6a6a6; border-width: thin;">
      <a class="page-link" href="{{ $paginator->nextPageUrl() }}" aria-label="Next" style="vertical-align: middle; display: table-cell; border-color: #a6a6a6;">
        <span aria-hidden="true"><b>&raquo;</b></span>
        <span class="sr-only"><b>Next</b></span>
      </a>
    </li>
@else
<li class="page-item disabled" style="border-style: solid; display: table; border-color: #a6a6a6; border-width: thin;">
      <a class="page-link" href="{{ $paginator->nextPageUrl() }}" aria-label="Next" style="vertical-align: middle; display: table-cell; border-color: #a6a6a6;">
        <span aria-hidden="true"><b>&raquo;</b></span>
        <span class="sr-only"><b>Next</b></span>
      </a>
    </li>
@endif
</ul>