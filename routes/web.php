<?php

use App\Http\Middleware\AdminMiddleware;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});


Route::resource('equipe', 'EquipeController');
Route::resource('joueur', 'JoueurController');
Route::resource('stats', 'StatsController');

Auth::routes();

Route::get('/home', function () {
    return view('post_equipe.home');
});

Route::group(['middleware' => AdminMiddleware::class], function (
    ) {
        Route::resource('admin', 'AdminController');
});

Route::get('/lang/{langue}', 'LangueController@index');